#!/usr/bin/env python3
from lammps import lammps
from mpi4py import MPI
import scipy.signal as sp
import numpy as np
import sys
from functools import partial
from multiprocessing import Pool
from datetime import datetime
from timeit import default_timer as timer
import pickle


comm = MPI.COMM_WORLD
rank=comm.Get_rank()
size=comm.Get_size()
argv = sys.argv
if len(argv) != 2:
    print("Syntax: base.py in.*")
    sys.exit()

infile = sys.argv[1]

start=timer()
if rank ==0:
    START = datetime.now()
    with open('JOBINFO','a') as f:
        dt_string = START.strftime('%d-%m-%Y %H:%M:%S')
        print(dt_string, 'START', file=f)

def ff_correlate(force,avgforce):
    m = force.shape[0]
    dforce   = force-avgforce
    factor   = np.arange(m,0,-1)[:m//2]
    ffcor    = sp.correlate(dforce,dforce,
                            mode='same',method='fft')[m//2:]/factor
    return ffcor


lmp = lammps(name='miru')
lmp.file(infile)
run1=int(lmp.extract_variable('equil'))
run2=int(lmp.extract_variable('prod'))
ani =int(lmp.extract_variable('animation'))
dt  =lmp.extract_global('dt')
RUNID = int(lmp.extract_variable('runid'))

if run2%2 !=0:
    print("Production run has to be even number of timesteps")
    exit()

# Equilibrate the system. No measurement.
lmp.command('run {}'.format(int(run1)))
lmp.command('reset_timestep 0')

lmp.command('compute cpos TOP com')
lmp.command('fix cpos0 TOP ave/time 1 {0} {0} c_cpos start 0 ave running mode vector'.format(int(run1)))
lmp.command('run {}'.format(int(run1)))
for I in range(1,4):
    lmp.command('variable cpos0{0} equal f_cpos0[{0}]'.format(int(I)))
    lmp.command('variable cpos{0} equal ${{cpos0{0}}}'.format(int(I)))
lmp.command('print "${cpos1} ${cpos2} ${cpos3}"')
lmp.command('unfix cpos0')
lmp.command('reset_timestep 0')
lmp.command('thermo {}'.format(int(run1//10)))
lmp.command('thermo_style custom step temp c_MDTEMP c_pe')
lmp.command('thermo_modify norm no')

#for I in range(30):
#    if I==0:
#        lmp.command('variable forcex{0} atom 3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+1)^2)^(-5)*(x-${{cpos1}})'.format(int(I)))
#        lmp.command('variable forcez{0} atom 3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+1)^2)^(-5)*(z-${{cpos3}}+1)'.format(int(I)))
#    else:
#        lmp.command('variable forcex{0} atom 3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+30^({0}/29))^2)^(-5)*(x-${{cpos1}})'.format(int(I)))
#        lmp.command('variable forcez{0} atom 3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+30^({0}/29))^2)^(-5)*(z-${{cpos3}}+30^({0}/29))'.format(int(I)))
#        lmp.command('compute tsforcex{0} TOP reduce sum v_forcex{0}'.format(int(I)))
#        lmp.command('compute tsforcez{0} TOP reduce sum v_forcez{0}'.format(int(I)))
#        lmp.command('fix storef{0} TOP vector 1 c_tsforcex{0} c_tsforcez{0}'.format(int(I)))

for i,I in enumerate(np.geomspace(1,5,30)):
    lmp.command('variable forcex{0} atom exp(-((x-${{cpos1}})^2+(y-${{cpos2}})^2)/{1}^2)*(3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+30)^2)^(-5)*(x-${{cpos1}})-2*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+30)^2)^(-3)*(x-${{cpos1}})/{1}^2)'.format(int(i),I))
    lmp.command('variable forcez{0} atom exp(-((x-${{cpos1}})^2+(y-${{cpos2}})^2)/{1}^2)*3*sqrt((x-${{cpos1}})^2+(y-${{cpos2}})^2+(z-${{cpos3}}+30)^2)^(-5)*(z-${{cpos3}}+30)'.format(int(i),I))
    #lmp.command('variable forcex{0} atom (2*(x-${{cpos1}})/({1}^2*(20-${{cpos3}})^3))*exp(-((x-${{cpos1}})^2+(y-${{cpos2}})^2)/{1}^2)'.format(int(i),I))
    #lmp.command('variable forcez{0} atom (3/((20-${{cpos3}})^4))*exp(-((x-${{cpos1}})^2+(y-${{cpos2}})^2)/{1}^2)'.format(int(i),I))
    lmp.command('compute tsforcex{0} TOP reduce sum v_forcex{0}'.format(int(i)))
    lmp.command('compute tsforcez{0} TOP reduce sum v_forcez{0}'.format(int(i)))
    lmp.command('fix storef{0} TOP vector 1 c_tsforcex{0} c_tsforcez{0}'.format(int(i)))

lmp.command('run {}'.format(int(run2)))

# result array shape is (30,run2)

def ext_fix(name,comp):
    global run2
    force=[]
    for J in range(run2):
        force.append(lmp.extract_fix('storef{}'.format(int(name)),0,2,J,int(comp)))
    force=np.array(force)
    avgf =np.mean(force)
    ffcor=ff_correlate(force,avgf)
    data = np.hstack((avgf,ffcor))
    return data

partial_x = partial(ext_fix, comp=0)
partial_z = partial(ext_fix, comp=1)

if rank==0:
    print('The pool size is {}'.format(size))
pool = Pool(size)
datax=np.array(pool.map(partial_x,range(30)))
dataz=np.array(pool.map(partial_z,range(30)))

for I in range(30):
    lmp.command('uncompute tsforcex{0}'.format(int(I)))
    lmp.command('uncompute tsforcez{0}'.format(int(I)))
    lmp.command('unfix storef{0}'.format(int(I)))

#lmp.command('dump PCA all custom 10 traj.{}.gz id type x y z'.format(RUNID))
#lmp.command('run {}'.format(ani))
#lmp.command('undump PCA')

#np.savetxt('forcex_{}.dat'.format(RUNID),datax)
#np.savetxt('forcez_{}.dat'.format(RUNID),dataz)

if rank ==0:
    with open('forcex_{}.pickle'.format(RUNID),'wb') as f:
        pickle.dump(datax,f)
    with open('forcez_{}.pickle'.format(RUNID),'wb') as f:
        pickle.dump(dataz,f)
    END = datetime.now()
    end = timer()
    elaps = end - start
    HOUR  = elaps//3600
    MIN  = (elaps - (HOUR*3600))//60
    SEC  = elaps - (HOUR*3600) - (MIN*60)
    with open('JOBINFO','a') as f:
        dt_string = END.strftime('%d-%m-%Y %H:%M:%S')
        print(dt_string, 'FINISH', 'WALLTIME:{0:.0f}h {1:.0f}m {2:.0f}s'.format(HOUR,MIN,SEC), file=f)
lmp.command('quit')
MPI.Finalize()
exit()
