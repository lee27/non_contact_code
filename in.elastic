log LOG.RUNID

units lj
dimension 3
boundary p p p
atom_style bond
bond_style dpd

# CREATE INITIAL SYSTEM
lattice sc 1.0

region REGION block @(-NN/2-(NN%2)*0.5) @(NN/2+(NN%2)*0.5) @(-NN/2-(NN%2)*0.5) @(NN/2+(NN%2)*0.5) -NZ 0
create_box 2 REGION bond/types 2 extra/bond/per/atom 18
create_atoms 1 box

variable layer atom "floor((z+0.001))"
variable xcor atom "floor((x+0.001))"
variable ycor atom "floor((y+0.001))"

## FROZEN group: bottom layer
variable tmp atom v_layer==-NZ
group FROZEN variable tmp
variable tmp delete
set group FROZEN type 2

# Select the top layer
variable tmp atom v_layer==0
group TOP variable tmp
variable tmp delete

# Storing parameters
variable equil equal EQUIL
variable prod equal PRODUCTION
variable gamma equal GAMMA
variable animation equal ANIMATION
variable runid equal RUNID

# SET LAMMPS RUNTIME LIMIT: clean exit after RUNTIME hours 
timer timeout RUNTIME:00:00 every @10E3

# mass / md timestep
mass * 1.0
timestep DT

variable cut equal 3

# SET INTERACTIONS
special_bonds lj/coul 0 1 1
pair_style zero ${cut}
pair_coeff * * ${cut}


comm_modify vel yes
neigh_modify once yes
bond_coeff 1 1 @(STIFF1/2) 0 0 TEMPERATURE ${gamma} SEED
bond_coeff 2 @sqrt(2) @(STIFF1/2) 0 0 TEMPERATURE ${gamma} SEED

# SET BONDS
group SUB type 1 2
create_bonds many SUB SUB 1 @1.0-TINY @1.0+TINY
create_bonds many SUB SUB 2 @sqrt(2)-TINY @sqrt(2)+TINY

# SET stochastic boundary condition
#fix SBC LANGEVIN langevin TEMPERATURE TEMPERATURE @TAU SEED tally no zero no

# MD group: everything __excluding__ bottom layer
group MD type 1 2

# INITIAL CONDITIONS / ASSIGN VARIABLES AND COMPUTES
# assign initial velocities to substrate particles, remove any com motion / global rotation
velocity MD create TEMPERATURE SEED mom yes rot yes loop geom

fix MD MD nve

# temperature of the MD group (standard 'temp' is too low, since this includes frozen particles)
compute MDTEMP MD temp
compute peratom MD pe/atom
compute pe all reduce sum c_peratom

write_data init.RUNID
