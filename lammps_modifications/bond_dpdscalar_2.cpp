/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://lammps.sandia.gov/, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Eric Simon (Cray)
------------------------------------------------------------------------- */

#include <cstring>
#include "bond_dpdscalar.h"

#include <cmath>
#include "atom.h"
#include "neighbor.h"
#include "comm.h"
#include "domain.h"
#include "force.h"
#include "memory.h"
#include "update.h"
#include "random_mars.h"
#include "error.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

BondDpdscalar::BondDpdscalar(LAMMPS *lmp) : Bond(lmp) {}

/* ---------------------------------------------------------------------- */

BondDpdscalar::~BondDpdscalar()
{
  if (copymode) return;

  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(r0);
    memory->destroy(k2);
    memory->destroy(k3);
    memory->destroy(k4);
    memory->destroy(gamma);
  }
}

/* ---------------------------------------------------------------------- */

void BondDpdscalar::compute(int eflag, int vflag)
{
  int i1,i2,n,type;
  double delx,dely,delz,ebond,fbond;
  double delvx,delvy,delvz,dot,randnum,sigma;
  double rsq,r,dr,dr2,dr3,dr4,de_bond;

  // stuff needed to compute purely harmonic interaction
  // pointers to perfect lattice positions
  double *x0 = atom->dvector[0];
  double *y0 = atom->dvector[1];
  double *z0 = atom->dvector[2];
  // we also need the boxsize
  double XX = domain->boxhi[0] - domain->boxlo[0];
  double YY = domain->boxhi[1] - domain->boxlo[1];
  double ZZ = domain->boxhi[2] - domain->boxlo[2];
  // extra variables
  double ax,ay,az,ux,uy,uz,fx,fy,fz,inp;

  ebond = 0.0;
  ev_init(eflag,vflag);

  double **x = atom->x;
  double **v = atom->v;
  double **f = atom->f;
  int **bondlist = neighbor->bondlist;
  int nbondlist = neighbor->nbondlist;
  int nlocal = atom->nlocal;
  int newton_bond = force->newton_bond;

  double tmp;
  double dtinvsqrt = 1.0/sqrt(update->dt);

  for (n = 0; n < nbondlist; n++) {

    i1 = bondlist[n][0];
    i2 = bondlist[n][1];
    type = bondlist[n][2];

    delx = x[i1][0] - x[i2][0];
    dely = x[i1][1] - x[i2][1];
    delz = x[i1][2] - x[i2][2];

    delvx = v[i1][0] - v[i2][0];
    delvy = v[i1][1] - v[i2][1];
    delvz = v[i1][2] - v[i2][2];

    // compute perfect lattice bond vector from i1 -> i2, don't forget periodic boundaries
    ax = x0[i1] - x0[i2]; while(ax>XX/2) ax-=XX; while(ax<-XX/2) ax+=XX;
    ay = y0[i1] - y0[i2]; while(ay>YY/2) ay-=YY; while(ay<-YY/2) ay+=YY;
    az = z0[i1] - z0[i2]; while(az>ZZ/2) az-=ZZ; while(az<-ZZ/2) az+=ZZ;
  
    // compute vector u = uj - ui
    ux = delx - ax; 
    uy = dely - ay; 
    uz = delz - az;

    // normalize lattice vector to unit vector 
    double L;
    L = sqrt(ax*ax+ay*ay+az*az); ax/=L; ay/=L; az/=L;
    inp = ax*ux + ay*uy + az*uz;

    // bond energy
    if (eflag) ebond=k2[type]*inp*inp;

    // this is not yet the virial, will deal with that later
    fbond=0.0;

    // DPD contribution
    tmp=fbond;
    if(gamma[type] > 0.0) {
      sigma = sqrt( 2.0 * force->boltz * temperature * gamma[type]);
      dot = ax*delvx + ay*delvy + az*delvz;
      randnum = random->gaussian();
      fbond -= gamma[type]*dot/(L*L);
      fbond += sigma*randnum*dtinvsqrt/L;
    }

    // apply force to each of 2 atoms
    fx = -2*k2[type]*ax*inp;
    fy = -2*k2[type]*ay*inp;
    fz = -2*k2[type]*az*inp;

    if (newton_bond || i1 < nlocal) {
      f[i1][0] += fx + ax*fbond;
      f[i1][1] += fy + ay*fbond;
      f[i1][2] += fz + az*fbond;
    }

    if (newton_bond || i2 < nlocal) {
      f[i2][0] -= fx + ax*fbond;
      f[i2][1] -= fy + ay*fbond;
      f[i2][2] -= fz + az*fbond;
    }

    // for virial/pressure computation, fbond should not contain dpdscalar part (!)
    fbond=tmp;
    if (evflag) ev_tally(i1,i2,nlocal,newton_bond,ebond,fbond,delx,dely,delz);
  }
}

/* ---------------------------------------------------------------------- */

void BondDpdscalar::allocate()
{
  allocated = 1;
  int n = atom->nbondtypes;

  memory->create(r0,n+1,"bond:r0");
  memory->create(k2,n+1,"bond:k2");
  memory->create(k3,n+1,"bond:k3");
  memory->create(k4,n+1,"bond:k4");
  memory->create(gamma,n+1,"bond:gamma");

  memory->create(setflag,n+1,"bond:setflag");
  for (int i = 1; i <= n; i++) setflag[i] = 0;
}

/* ----------------------------------------------------------------------
   set coeffs from one line in input script or data file
------------------------------------------------------------------------- */

void BondDpdscalar::coeff(int narg, char **arg)
{
  if (narg != 8) error->all(FLERR,"Incorrect args for bond coefficients");
  if (!allocated) allocate();

  int ilo,ihi;
  utils::bounds(FLERR,arg[0],1,atom->nbondtypes,ilo,ihi,error);

  // NOTE: only k2 is used here (!)
  double r0_one = utils::numeric(FLERR,arg[1],false,lmp);
  double k2_one = utils::numeric(FLERR,arg[2],false,lmp);
  double k3_one = utils::numeric(FLERR,arg[3],false,lmp);
  double k4_one = utils::numeric(FLERR,arg[4],false,lmp);

  // temperature and seed are global, i.e. the same for all bond types; gamma can be different
  temperature = utils::numeric(FLERR,arg[5],false,lmp);
  double gamma_one = utils::numeric(FLERR,arg[6],false,lmp);
  seed = utils::numeric(FLERR,arg[7],false,lmp);

  if(comm->me==0) {
    if(screen) fprintf(screen,"DPDSCALAR_BOND: TEMPERATURE=%lf GAMMA=%lf SEED=%i\n",temperature,gamma_one,seed);
    if(logfile) fprintf(logfile,"DPDSCALAR_BOND: TEMPERATURE=%lf GAMMA=%lf SEED=%i\n",temperature,gamma_one,seed);
  }

  // initialize Marsaglia RNG with processor-unique seed
  if (seed <= 0) error->all(FLERR,"Illegal seed value");
  random = new RanMars(lmp,seed + comm->me);

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    r0[i] = r0_one;
    k2[i] = k2_one;
    k3[i] = k3_one;
    k4[i] = k4_one;
    gamma[i] = gamma_one;
    setflag[i] = 1;
    count++;
  }

  if (count == 0) error->all(FLERR,"Incorrect args for bond coefficients");

  // Copy current atom positions to dvector storage arrays declared with fix property/atom in the script
  // It is assumed the atoms here are all in their perfect lattice positions (!)

  if( atom->dvector == NULL ) error->all(FLERR,"Use fix property/atom to define three extra properties, as doubles");

  double **x = atom->x;
  int nlocal = atom->nlocal;
  for (int i = 0; i < nlocal; i++) {
    atom->dvector[0][i] = x[i][0];
    atom->dvector[1][i] = x[i][1];
    atom->dvector[2][i] = x[i][2];
  }
}

/* ----------------------------------------------------------------------
   return an equilbrium bond length
------------------------------------------------------------------------- */

double BondDpdscalar::equilibrium_distance(int i)
{
  return r0[i];
}

/* ----------------------------------------------------------------------
   proc 0 writes out coeffs to restart file
------------------------------------------------------------------------- */

void BondDpdscalar::write_restart(FILE *fp)
{
  fwrite(&r0[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&k2[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&k3[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&k4[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&gamma[1],sizeof(double),atom->nbondtypes,fp);
}

/* ----------------------------------------------------------------------
   proc 0 reads coeffs from restart file, bcasts them
------------------------------------------------------------------------- */

void BondDpdscalar::read_restart(FILE *fp)
{
  allocate();

  if (comm->me == 0) {
    utils::sfread(FLERR,&r0[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&k2[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&k3[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&k4[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&gamma[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
  }
  MPI_Bcast(&r0[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&k2[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&k3[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&k4[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&gamma[1],atom->nbondtypes,MPI_DOUBLE,0,world);

  for (int i = 1; i <= atom->nbondtypes; i++) setflag[i] = 1;
}

/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void BondDpdscalar::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->nbondtypes; i++)
    fprintf(fp,"%d %g %g %g %g %g\n",i,r0[i],k2[i],k3[i],k4[i],gamma[i]);
}

/* ---------------------------------------------------------------------- */

double BondDpdscalar::single(int type, double rsq, int /*i*/, int /*j*/, double &fforce)
{
  error->all(FLERR,"ERROR: this routine was not modified for DPDSCALAR\n");

  double r = sqrt(rsq);
  double dr = r - r0[type];
  double dr2 = dr*dr;
  double dr3 = dr2*dr;
  double dr4 = dr3*dr;
  double de_bond = 2.0*k2[type]*dr + 3.0*k3[type]*dr2 + 4.0*k4[type]*dr3;
  if (r > 0.0) fforce = -de_bond/r;
  else fforce = 0.0;
  return (k2[type]*dr2 + k3[type]*dr3 + k4[type]*dr4);
}

/* ---------------------------------------------------------------------- */

void *BondDpdscalar::extract(const char *str, int &dim)
{
  dim = 1;
  if (strcmp(str,"r0")==0) return (void*) r0;
  return nullptr;
}
