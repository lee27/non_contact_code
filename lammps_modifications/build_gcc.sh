module purge
module load cmake gcc openmpi anaconda3
cmake ../cmake -C ../cmake/presets/gcc.cmake -D LAMMPS_EXCEPTIONS=on -D PKG_PYTHON=yes -D PKG_MOLECULE=yes -D BUILD_SHARED_LIBS=on
make -j 10
make install-python
