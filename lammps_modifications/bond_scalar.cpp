/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   https://lammps.sandia.gov/, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Eric Simon (Cray)
------------------------------------------------------------------------- */

#include <cstring>
#include "bond_scalar.h"

#include <cmath>
#include "atom.h"
#include "neighbor.h"
#include "comm.h"
#include "force.h"
#include "memory.h"
#include "update.h"
#include "random_mars.h"
#include "error.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

BondScalar::BondScalar(LAMMPS *lmp) : Bond(lmp) {}

/* ---------------------------------------------------------------------- */

BondScalar::~BondScalar()
{
  if (copymode) return;

  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(r0);
    memory->destroy(k1);
    memory->destroy(k2);
    memory->destroy(gamma1);
    memory->destroy(gamma2);
  }
}

/* ---------------------------------------------------------------------- */

void BondScalar::compute(int eflag, int vflag)
{
  int i1,i2,n,type;
  double delx,dely,delz,ebond;
  double delvx,delvy,delvz,randnum,sigma1,sigma2;
  double rsq,r,lx,ly,lz;
  double fbondx,fbondy,fbondz;
  double xpara,ypara,zpara,xperp,yperp,zperp;
  double vxpara,vypara,vzpara,vxperp,vyperp,vzperp;

  ebond = 0.0;
  ev_init(eflag,vflag);

  double **x = atom->x;
  double **v = atom->v;
  double **f = atom->f;
  int **bondlist = neighbor->bondlist;
  int nbondlist = neighbor->nbondlist;
  int nlocal = atom->nlocal;
  int newton_bond = force->newton_bond;

  double tmpx,tmpy,tmpz;
  double dtinvsqrt = 1.0/sqrt(update->dt);

  for (n = 0; n < nbondlist; n++) {
    i1 = bondlist[n][0];
    i2 = bondlist[n][1];
    type = bondlist[n][2];

    delx = x[i1][0] - x[i2][0];
    dely = x[i1][1] - x[i2][1];
    delz = x[i1][2] - x[i2][2];

    // define the lattice vector
    lx   = floor(delx+0.5);
    ly   = floor(dely+0.5);
    lz   = floor(delz+0.5);

    delvx = v[i1][0] - v[i2][0];
    delvy = v[i1][1] - v[i2][1];
    delvz = v[i1][2] - v[i2][2];

    rsq = delx*delx + dely*dely + delz*delz;
    r = sqrt(rsq);

    // _para(llel) and _perp(endicular) are mutually exclusive
    xpara = (delx-r0[type])*lx;
    ypara = (dely-r0[type])*ly;
    zpara = (delz-r0[type])*lz;

    xperp  = delx*(1-lx);
    yperp  = dely*(1-ly);
    zperp  = delz*(1-lz);

    vxpara = delvx*lx;
    vypara = delvy*ly;
    vzpara = delvz*lz;

    vxperp  = delvx*(1-lx);
    vyperp  = delvy*(1-ly);
    vzperp  = delvz*(1-lz);

    // force & energy
    if (r > 0.0){
    fbondx  = -k1[type]*xpara-k2[type]*xperp;
    fbondy  = -k1[type]*ypara-k2[type]*yperp;
    fbondz  = -k1[type]*zpara-k2[type]*zperp;
    }
    else {
        fbondx = 0.0;
        fbondy = 0.0;
        fbondz = 0.0;
    }

    if (eflag) {
        ebond  = 0.5*k1[type]*(xpara*xpara+ypara*ypara+zpara*zpara);
        ebond += 0.5*k2[type]*(xperp*xperp+yperp*yperp+zperp*zperp);
    }

    // DPD contribution

    tmpx=fbondx;
    tmpy=fbondy;
    tmpz=fbondz;
    if (gamma1[type]>0.0||gamma2[type]>0.0) {
      sigma1  = sqrt(2.0 * force->boltz * temperature * gamma1[type]);
      sigma2  = sqrt(2.0 * force->boltz * temperature * gamma2[type]);
      randnum = random->gaussian();
      fbondx += -gamma1[type]*vxpara-gamma2[type]*vxperp;
      fbondy += -gamma1[type]*vypara-gamma2[type]*vyperp;
      fbondz += -gamma1[type]*vzpara-gamma2[type]*vzperp;
      fbondx += sigma1*randnum*dtinvsqrt*lx;
      fbondy += sigma1*randnum*dtinvsqrt*ly;
      fbondz += sigma1*randnum*dtinvsqrt*lz;
      fbondx += sigma2*randnum*dtinvsqrt*(1-lx);
      fbondy += sigma2*randnum*dtinvsqrt*(1-ly);
      fbondz += sigma2*randnum*dtinvsqrt*(1-lz);
      if (eflag) {
        ebond += 0.5*gamma1[type]*(vxpara*vxpara+vypara*vypara+vzpara*vzpara);
        ebond += 0.5*gamma2[type]*(vxperp*vxperp+vyperp*vyperp+vzperp*vzperp);
      }
    }

    // apply force to each of 2 atoms

    if (newton_bond || i1 < nlocal) {
      f[i1][0] += fbondx;
      f[i1][1] += fbondy;
      f[i1][2] += fbondz;
    }

    if (newton_bond || i2 < nlocal) {
      f[i2][0] -= fbondx;
      f[i2][1] -= fbondy;
      f[i2][2] -= fbondz;
    }

    // for virial/pressure computation, fbond should not contain dpd part (!)
    fbondx=tmpx;
    if (evflag) ev_tally(i1,i2,nlocal,newton_bond,ebond,fbondx,delx,dely,delz);
  }
}

/* ---------------------------------------------------------------------- */

void BondScalar::allocate()
{
  allocated = 1;
  int n = atom->nbondtypes;

  memory->create(r0,n+1,"bond:r0");
  memory->create(k1,n+1,"bond:k1");
  memory->create(k2,n+1,"bond:k2");
  memory->create(gamma1,n+1,"bond:gamma1");
  memory->create(gamma2,n+1,"bond:gamma2");

  memory->create(setflag,n+1,"bond:setflag");
  for (int i = 1; i <= n; i++) setflag[i] = 0;
}

/* ----------------------------------------------------------------------
   set coeffs from one line in input script or data file
------------------------------------------------------------------------- */

void BondScalar::coeff(int narg, char **arg)
{
  if (narg != 8) error->all(FLERR,"Incorrect args for bond coefficients");
  if (!allocated) allocate();

  int ilo,ihi;
  utils::bounds(FLERR,arg[0],1,atom->nbondtypes,ilo,ihi,error);

  double r0_one = utils::numeric(FLERR,arg[1],false,lmp);
  double k1_one = utils::numeric(FLERR,arg[2],false,lmp);
  double k2_one = utils::numeric(FLERR,arg[3],false,lmp);

  // temperature and seed are global, i.e. the same for all bond types; gamma can be different
  temperature = utils::numeric(FLERR,arg[4],false,lmp);
  double gamma1_one = utils::numeric(FLERR,arg[5],false,lmp);
  double gamma2_one = utils::numeric(FLERR,arg[6],false,lmp);
  seed = utils::numeric(FLERR,arg[7],false,lmp);

  if(comm->me==0) {
    if(screen) fprintf(screen,"DPD_BOND: TEMPERATURE=%lf GAMMA1=%lf GAMMA2=%lf SEED=%i\n",temperature,gamma1_one,gamma2_one,seed);
    if(logfile) fprintf(logfile,"DPD_BOND: TEMPERATURE=%lf GAMMA1=%lf GAMMA2=%lf SEED=%i\n",temperature,gamma1_one,gamma2_one,seed);
  }

  // initialize Marsaglia RNG with processor-unique seed
  if (seed <= 0) error->all(FLERR,"Illegal seed value");
  random = new RanMars(lmp,seed + comm->me);

  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    r0[i] = r0_one;
    k1[i] = k1_one;
    k2[i] = k2_one;
    gamma1[i] = gamma1_one;
    gamma2[i] = gamma2_one;
    setflag[i] = 1;
    count++;
  }

  if (count == 0) error->all(FLERR,"Incorrect args for bond coefficients");
}

/* ----------------------------------------------------------------------
   return an equilbrium bond length
------------------------------------------------------------------------- */

double BondScalar::equilibrium_distance(int i)
{
  return r0[i];
}

/* ----------------------------------------------------------------------
   proc 0 writes out coeffs to restart file
------------------------------------------------------------------------- */

void BondScalar::write_restart(FILE *fp)
{
  fwrite(&r0[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&k1[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&k2[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&gamma1[1],sizeof(double),atom->nbondtypes,fp);
  fwrite(&gamma2[1],sizeof(double),atom->nbondtypes,fp);
}

/* ----------------------------------------------------------------------
   proc 0 reads coeffs from restart file, bcasts them
------------------------------------------------------------------------- */

void BondScalar::read_restart(FILE *fp)
{
  allocate();

  if (comm->me == 0) {
    utils::sfread(FLERR,&r0[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&k1[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&k2[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&gamma1[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
    utils::sfread(FLERR,&gamma2[1],sizeof(double),atom->nbondtypes,fp,nullptr,error);
  }
  MPI_Bcast(&r0[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&k1[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&k2[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&gamma1[1],atom->nbondtypes,MPI_DOUBLE,0,world);
  MPI_Bcast(&gamma2[1],atom->nbondtypes,MPI_DOUBLE,0,world);

  for (int i = 1; i <= atom->nbondtypes; i++) setflag[i] = 1;
}

/* ----------------------------------------------------------------------
   proc 0 writes to data file
------------------------------------------------------------------------- */

void BondScalar::write_data(FILE *fp)
{
  for (int i = 1; i <= atom->nbondtypes; i++)
    fprintf(fp,"%d %g %g %g %g %g\n",i,r0[i],k1[i],k2[i],gamma1[i],gamma2[i]);
}

/* ---------------------------------------------------------------------- */

double BondScalar::single(int type, double rsq, int /*i*/, int /*j*/, double &fforce)
{
  error->all(FLERR,"ERROR: this routine was not modified for DPD\n");

  double r = sqrt(rsq);
  double dr = r - r0[type];
  double dr2 = dr*dr;
  double de_bond = k1[type]*dr;
  if (r > 0.0) fforce = -de_bond/r;
  else fforce = 0.0;
  return (0.5*k1[type]*dr2);
}

/* ---------------------------------------------------------------------- */

void *BondScalar::extract(const char *str, int &dim)
{
  dim = 1;
  if (strcmp(str,"r0")==0) return (void*) r0;
  return nullptr;
}
