/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef BOND_CLASS

BondStyle(probe,BondProbe)

#else

#ifndef LMP_BOND_PROBE_H
#define LMP_BOND_PROBE_H

#include "bond.h"

namespace LAMMPS_NS {

class BondProbe : public Bond {
 public:
  BondProbe(class LAMMPS *);
  virtual ~BondProbe();
  virtual void compute(int, int);
  virtual void coeff(int, char **);
  double equilibrium_distance(int);
  void write_restart(FILE *);
  virtual void read_restart(FILE *);
  void write_data(FILE *);
  double single(int, double, int, int, double &);
  virtual void *extract(const char *, int &);

 protected:
  double *r0,*k2,*k3,*k4;
  void allocate();
  // dpd additions
  int seed;
  double temperature,*gamma;
  class RanMars *random;
  // probe additions
  double A,N,Z;
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Incorrect args for bond coefficients

Self-explanatory.  Check the input script or data file.

*/
