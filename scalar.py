#!/usr/bin/env python3
from lammps import lammps
from mpi4py import MPI
import scipy.signal as sp
import numpy as np
import sys
from functools import partial
from multiprocessing import Pool
from datetime import datetime
from timeit import default_timer as timer
import pickle


comm = MPI.COMM_WORLD
rank=comm.Get_rank()
size=comm.Get_size()
argv = sys.argv
if len(argv) != 2:
    print("Syntax: base.py in.*")
    sys.exit()

infile = sys.argv[1]

start=timer()
if rank ==0:
    START = datetime.now()
    with open('JOBINFO','a') as f:
        dt_string = START.strftime('%d-%m-%Y %H:%M:%S')
        print(dt_string, 'START', file=f)

def ff_correlate(force,avgforce):
    m = force.shape[0]
    dforce   = force-avgforce
    factor   = np.arange(m,0,-1)[:m//2]
    ffcor    = sp.correlate(dforce,dforce,
                            mode='same',method='fft')[m//2:]/factor
    return ffcor


lmp = lammps(name='miru')
lmp.file(infile)
run1=int(lmp.extract_variable('equil'))
run2=int(lmp.extract_variable('prod'))
ani =int(lmp.extract_variable('animation'))
dt  =lmp.extract_global('dt')
RUNID = int(lmp.extract_variable('runid'))

if run2%2 !=0:
    print("Production run has to be even number of timesteps")
    exit()

# Equilibrate the system. No measurement.
lmp.command('run {}'.format(int(run1)))
lmp.command('reset_timestep 0')

lmp.command('dump PCA all custom 10 traj.{}.gz id type x y z'.format(RUNID))
lmp.command('run {}'.format(ani))
lmp.command('undump PCA')

lmp.command('quit')
MPI.Finalize()
exit()
