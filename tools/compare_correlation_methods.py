#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

'''
Read in a timeseries, compute autocorrelation in cyclic and non-cyclic way
using FFT methods. For non-cyclic, time series length must be __even__ (!)
'''

# functions below compute correlaton directly from the definition = SLOW (!)
# ts = timeseries; s = shift

def cyclic(ts,s):
  n=ts.size
  sum=nrm=0
  for i in range(n):
    j=i+s
    # make it cyclic (!)
    if j>=n: j-=n
    sum+=ts[i]*ts[j]
    nrm+=1
  return sum/nrm

def noncyclic(ts,s):
  n=ts.size
  sum=nrm=0
  for i in range(n):
    j=i+s
    # non-cyclic; only consider pairs inside the series
    if j<n:
      sum+=ts[i]*ts[j]
      nrm+=1
  return sum/nrm

# create a fake time series
ts = np.random.rand(20)
ts -= np.mean(ts)

# DO COMPARISONS

# non-cyclic
n = ts.size
factor = np.arange(n,0,-1)[:n//2]
ac = signal.correlate(ts,ts,mode='same',method='fft')[n//2:]/factor
print('non-cyclic')
for i in range(ac.size): print(i,noncyclic(ts,i),ac[i])

# cyclic
n = ts.size
ac = np.absolute(np.fft.fft(ts))**2
ac = np.real(np.fft.ifft(ac)) / n
ac = ac[:n//2]
print('cyclic')
for i in range(ac.size): print(i,cyclic(ts,i),ac[i])
