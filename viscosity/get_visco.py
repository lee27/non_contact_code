#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

'''
Read in correlation function, compute bulk and shear viscosity. Tries to do it
somewhat accurately by applying a vertical shift.

ARGS: <file containing pressure correlation = cnorm.n>

Output is single line written to file = gamma: GAMMA BULK SHEAR
'''

def get_param(fname,str):
  chk = "#{}".format(str)
  fp=open(fname)
  for line in fp:
    field=line.strip().split('=')
    if field[0]==chk:
      fp.close()
      return field[1]
  print('ERROR: file',fname,'does not contain variable',str)
  exit()

# get run parameters
DT=float(get_param('PARAM','DT'))
VOL=float(get_param('PARAM','LL'))**3
T=float(get_param('PARAM','TEMPERATURE'))
GAMMA=float(get_param('PARAM','GAMMA'))
print('run parameters: dt, volume, temperature:',DT,VOL,T)

# helper function: minimization of this function yields optimal vertical shift
# delta = shift, c = correlation function
def slope(delta,c):
  min = int(0.25*len(c))
  y = np.cumsum(c-delta) [min:]
  n=len(y)
  x=np.linspace(0,1,n)
  x-=np.mean(x)
  y-=np.mean(y)
  s = np.sum(x*y) / np.sum(x*x)
  return s**2

# analyze both correlation functions

output=[GAMMA]

for i in range(2):
  c=np.loadtxt(sys.argv[1],usecols=i)
  result = optimize.minimize_scalar(slope,args=(c))
  if not result.success: sys.exit('ERROR: could not find optimal shift')
  delta=result.x
  print('optimal shift:',delta)
  c-=delta
  # this is Eq. 2 from Ladd paper
  eta = VOL * DT * np.cumsum(c) / T
  # take average of tail as best estimate of viscosity
  cut = int(0.5*len(eta))
  best = np.mean( eta[cut:] )
  output.append(best)
  # show plot of integral running value, hopefully you see a plateau
  # plt.plot(eta)
  # plt.axhline(best,c='r')
  # plt.show()

# save and exit
print(output)
sys.stdout = open('gamma','w')
print(output[0],output[1],output[2])


