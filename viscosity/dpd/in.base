log LOG.RUNID
JOBINFO START

# do bulk simulation, measure pressure correlations

units lj
dimension 3
boundary p p p
atom_style bond

# SET LAMMPS RUNTIME LIMIT: clean exit after RUNTIME hours 
timer timeout RUNTIME:00:00 every @10E3

# MIRU x-tal = simple cubic
lattice sc 1.0 origin 0.5 0.5 0.5
region REGION block 0 LL 0 LL 0 LL
create_box 1 REGION bond/types 2 extra/bond/per/atom 18
create_atoms 1 box
region REGION delete

# mass / md timestep
mass * 1.0
timestep DT

# create nn and nnn harmonic bonds
pair_style zero 1.5
pair_coeff * *
bond_style dpd
comm_modify vel yes
bond_coeff 1 1 $(STIFF1/2) 0 0 TEMPERATURE GAMMA SEED
bond_coeff 2 $(sqrt(2)) $(STIFF1/2) 0 0 TEMPERATURE GAMMA SEED
create_bonds many all all 1 0.99 1.01
create_bonds many all all 2 $(sqrt(2)-0.01) $(sqrt(2)+0.01)
# should be safe, makes things faster
neigh_modify exclude type 1 1 once yes

write_data INIT.RUNID

# EQUILIBRATE

fix NVE all nve

# total pressure, should equal press of thermo output
variable pp equal (c_thermo_press[1]+c_thermo_press[2]+c_thermo_press[3])/3
# shear pressure
variable ps equal (c_thermo_press[4]+c_thermo_press[5]+c_thermo_press[6])/3

thermo 1000
thermo_style custom step temp press v_ps
thermo_modify norm no

run @EQUIL

# PRODUCTION STAGE

thermo @NEVERY

CC CALL PYTHON FROM LAMMPS: Special lammps compilation required! Inside lammps 
directory: /lib/python, replace Makefile.lammps with python3 version. Verify 
command 'python-config' exists on your system. Finally, in the src dir:
  make yes-python
  make mpi
  make mpi mode=shlib
  make install-python

Debugging python inside lammps is tricky, as error messages are not displayed. 
Common source of errors are missing import libraries. Print statements need to 
be flushed: print(x,flush=True). Print output does not appear in the logfile!

Below we define a python function 'myfunc' to scan the contents of 'fix vector' 
and average the correlation functions. The lammps/python interface uses a 
wrapper to pass the function arguments = stuff following the 'input' keyword:
  $0 = number of arguments (an integer, you must set it yourself, cannot be deduced)
  $1 = RUNID, passed as string, code=s
  $2 = SELF, pointer to lammps instance, code=p
  $3 = fix vector ID, as string, code=s
  $4 = number of rows being read, integer, code=i
  $5 = number of columns being read, integer, code=i
  $6 = whether to use Miru method to compute correlation
The argument types are specified after the 'format' keyword and encoded as a 
single string (except for $0, whose type does not get specified).

The correlation is written to file = correlation.RUNID = 1-column text file. 
First number is number of correlation functions N in the average; following 
rows entries are X-correlation, then comes Z. CC

python myfunc input 6 RUNID SELF STORAGE @NEVERY COLS MIRU format spsiii here """
def myfunc(runid,lmpptr,fixname,rows,cols,miru):
  import sys,os
  # NEW: get rank from suitable environment variable
  try: rank = int(os.getenv('SLURM_PROCID'))
  except:
    try: rank = int(os.getenv('OMPI_COMM_WORLD_RANK'))
    except: sys.exit('ERROR: could not get rank in python function')
  # beware: all processors execute the function; only one should do the work...
  if rank==0:
    import os.path
    import numpy as np
    import scipy.signal as sp
    from lammps import lammps
    lmp=lammps(ptr=lmpptr)
    # read current data if present, otherwise start from scratch
    outfile='correlation.'+runid
    if not os.path.isfile(outfile): data=np.zeros(1+(rows//2)*cols)
    else: data=np.loadtxt(outfile)
    # copy data from fix, one row at a time
    cur=[]
    for i in range(rows):
      ll=[]
      for j in range(cols):
        ll.append( lmp.extract_fix(fixname,0,2,i,j) )
      cur.append(ll)
    cur=np.asarray(cur,dtype=float).T
    # compute autocorrelations, add to average
    data[0] += 1
    for i in range(cols):
      cor=cur[i]
      cor-=np.mean(cor)
      if miru==0:
        cor = np.fft.fft( cor ) * np.conj(np.fft.fft( cor ))
        cor = np.real(np.fft.ifft(cor)) / rows
        cor = cor[:rows//2]
      else:
        factor = np.arange(rows,0,-1)[:rows//2]
        cor = sp.correlate(cor,cor,mode='same',method='fft')[rows//2:]/factor
      lo=1+i*(rows//2); up=lo+(rows//2); data[lo:up] += cor
    # save it
    np.savetxt(outfile,data)
    # for convenience, also create file suitable for direct plotting
    data = np.asarray(np.array_split(data[1:],cols)/data[0])
    np.savetxt('cnorm.'+runid,data.T)
"""

# make sure any old data gets deleted
shell "rm -f correlation.RUNID"

fix STORAGE all vector 1 v_pp v_ps

run @PRODUCTION every @NEVERY &
  "python myfunc invoke" &
  "unfix STORAGE" &
  "fix STORAGE all vector 1 v_pp v_ps"

JOBINFO FINISH
