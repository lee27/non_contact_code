#!/usr/bin/env python3

'''
VERSION: 04.12.2019

NEW: Now takes care not to mess with embedded python code

PURPOSE: reads a "human-readable" LAMMPS script, making replacements of 
keywords by numerical values from a "dictionary" file PARAM. Each 
keyword must start with a CAPITAL letter, and may only contain CAPITALS 
and/or NUMBERS thereafter (e.g. XX, TEMP4 are valid; 4X2, Btemp are 
not). The PARAM file must be located in the same directory the script is 
run from. The replacement "real" LAMMPS script is called "in", and is 
created in the same directory.

ARGS: HUMAN_READABLE_LAMMPS_SCRIPT optional:RUNID
EG: ../in.base
EG: ../in.base 734

The PARAM file is a text file containing lines of the form: #VAR=VAL

Whenever the string VAR is found in the HUMAN_READABLE_LAMMPS_SCRIPT, it 
is replaced by VAL. The script should be smart enough to work with 
"overlapping" VAR names (e.g. TEMP/TEMPER). The script also checks if 
any of the user assigned IDs for groups/fixes/etc overlap with names in 
the PARAM file, which I consider is a "bad" thing. When this happens, an 
error is generated, and the script terminates. The PARAM file may 
contain empty lines. In case several rules for the same VAR are given, 
only the first one is used, and a warning is printed.

Also allowed are simple math formulas, which need to start with '@' and 
end with a space or EOL. For example: @sin(5.0) gets replaced by 
-0.958924274663138, and so forth. Formulas from the python math/random
modules should work. Formulas may also contain KEYWORDS from the 
PARAM file, as long as the replaced string @XXX can be evaluated to a 
numerical value.

The following keywords in the "human-readable" lammps file are special:

SEED: this is replaced by a large random integer whenever it is 
encountered, which is useful for lammps functions that require a random 
seed as input.

RUNID: The script checks for filenames of the form LOG.RUNID, with 
integer RUNID>=0, until it finds one that does not yet exist. If you 
discipline yourself and your scripts such that the lammps logfile is 
consistently named LOG.RUNID, this ensures no logfiles get overwritten 
when starting another simulation from the same directory at some later 
time (WARNING: this does not work when submitting jobs simultaneously, 
as there will be a race condition that is beyond the scope of this 
script to handle). In the same way, you could use RUNID in your lammps 
script to generate filenames.

NEW: You can also provide a RUNID of your choice as second argument of 
the script; this can be useful if you provide many jobs simultaneously.

CC: Whenever a line starts with the string 'CC' the COMMENT MODE is activated,
meaning that the lines following will be ignored completely, until the next
instance of 'CC' is encountered.

JOBINFO: Whenever "JOBINFO XXX" appears in the "human-readable" lammps 
file, the real lammps script will print the string "RUNID DATE XXX" to a 
file called JOBINFO, with DATE replaced by the actual system time. This 
is useful to track the progress of runs. I typically put a "JOBINFO 
FINISH" as last line. If the job finished successfully, it must appear 
in the JOBINFO file.
'''

import sys,re,os,os.path,glob

# imported like this, you can call the corresponding functions directly, without prepending the module name
from math import *
from random import *

if len(sys.argv)==1:
  print(__doc__)
  exit()

# SCAN param file, dump variable names and values in global lists
def read_param(file):
  # NOT NEEDED TO ANNOUNCE THESE AS GLOBAL, APPARENTLY: global DIC,VAL
  for line in open(file):
    f=line.strip().split('=')
    if(len(f)==2 and f[0][0]=='#'):
      # strip leading '#' character from variable name
      w=f[0][1:]
      if w not in DIC:
        print('eval.py: read_param:',w,f[1],file=sys.stderr)
        DIC.append(w)
        VAL.append(f[1])
      else: print('eval.py: read_param: WARNING: ignoring duplicate entry',w,f[1],file=sys.stderr)

# CONVERT the match @XXX to numerical value by evaluating the XXX part using python math functions
def evaluate_formula(m):
  # strip leading '@' character of match
  w=m.group()[1:]
  # evaluate the string numerically
  try:
    val=float(eval(w))
  except (SyntaxError,NameError,TypeError,ZeroDivisionError):
    os.system("rm -f in")
    print('eval.py: UNKNOWN FUNCTION:',w,file=sys.stderr)
    exit()
  print('eval.py: computing:',w,'->',val,file=sys.stderr)
  # return string, formatted according to whether value is integer or float; NOTE TRAILING SPACE AT END (!)
  if val==int(val): return "{:.0f} ".format(val)
  else: return "{:.12e} ".format(val)

# REPLACE matched string with value from dictionary
def do_sub(m):
  # matched string
  w=m.group()
  # if matched string is not in dictionary return it unchanged
  if w not in DIC: return w
  # return large random integer for SEED, otherwise the dictionary value
  if w=='SEED': ret=str(randint(13011975,19092020))
  else: ret=VAL[ DIC.index(w) ]
  print('eval.py: replacement:',w,'->',ret,file=sys.stderr)
  return ret

# DRIVER ROUTINE, processes a single line of my lammps script
def process_line(line):
  # CC must be announced as global, otherwise we cannot modify it; does not appear to apply to lists though (!)
  global CC,PYTHON
  line=line.rstrip() 
  # 07.12.2019: do not touch python code (!)
  if '"""' in line:
    PYTHON*=-1
  elif PYTHON==1:
    print(line)
    return
  # Proceed as normal:
  line=line.strip()
  f=line.split()
  # catch empty lines
  if len(f)==0:
    print()
    return
  # switch comment mode
  if f[0]=='CC': 
    CC*=-1
    return
  # COMMENT MODE: do not print line (!)
  if CC==1: 
    #print('#REMOVED_COMMENT_BLOCK')
    return
  # SET macro JOBINFO, used to track run progress
  if f[0]=='JOBINFO':
    print(r'shell "echo',RUNID,r'$(date +%d-%b-%Y-%T)',f[1],r'>> JOBINFO"')
    return
  # LAMMPS comment lines are printed VERBATIM
  if f[0]=='#': 
    print(line)
    return
  # CHECK IF VARIABLE NAME COLLIDES WITH ANY OF THE LAMMPS USER-ASSIGNED IDS FOR FIXES/COMPUTES/ETC (!)
  if (f[0] in LKW) and (f[1] in DIC):
    print('#FATAL: eval.py: parameter name collides with:',f[0],f[1],file=sys.stderr)
    os.system("rm -f in")
    exit()
  # SCAN LINE: regex matches words that start with a CAPITAL and contain CAPITALS or NUMBERS thereafter; routine "do_sub" returns the replacement string
  line=re.sub(r'[A-Z][A-Z0-9]*',do_sub,line)
  # SCAN LINE: evaluate math formulas
  # regex: match strings that begin with the character '@', followed by arbitrary length sequence of non-space characters, ending with space or END-OF-LINE
  line=re.sub(r'@\S*(\s|$)',evaluate_formula,line)
  # FINAL LINE AFTER ALL CONVERSIONS HAVE BEEN PERFORMED
  print(line.rstrip())

# set RUNID by trying filenames of the form 'LOG.runid' until a free one is found
def get_runid(): 
  runid=0
  while True:
    if os.path.exists( 'LOG.{}'.format(runid) )==False:
      print('eval.py: SETTING RUNID:',runid,file=sys.stderr)
      return str(runid)
    runid += 1

################
# MAIN PROGRAM #
################

# lammps keywords that require some care when replacing variables
LKW = ['fix','group','dump','compute','variable','python']

# set empty dictionary and value lists 
DIC=[]
VAL=[]

# populate lists with entries from param file
read_param('PARAM')

# add RUNID to dictionary, also make it global
if len(sys.argv)==3: RUNID=sys.argv[2]  
else: RUNID=get_runid()

DIC.append('RUNID')
VAL.append(RUNID)

# add special keyword SEED to dictionary, which gets replaced by a random integer whenever it is called
DIC.append('SEED')

# SET comment-mode and python mode initially to "off"
CC=int(-1)
PYTHON=int(-1)

# SCAN THE FILE, output is written to file "in"
sys.stdout = open('in.'+RUNID,'w')
for line in open(sys.argv[1]): process_line(line)
sys.stdout = sys.__stdout__

print('eval.py: DONE PREPARING LAMMPS FILE',file=sys.stderr)
