#!/usr/bin/env python3

import sys,glob,getpass,os
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy import integrate

# helper function to read run parameters
def get_param(fname,str):
  chk = "#{}".format(str)
  fp=open(fname)
  for line in fp:
    field=line.strip().split('=')
    if field[0]==chk:
      fp.close()
      print('PARAMETER',str,field[1])
      return field[1]
  print('ERROR: file',fname,'does not contain variable',str)
  exit()

## CODE starts here ##

# data directories to analyze
dirs=sys.argv[1:]

# setup the figure
n=len(dirs)
fig,_=plt.subplots(n,2,figsize=(12,3*n))

def analyze_datadir(dir,fignum):

  # get run parameters for this directory
  PFILE=dir+'/PARAM'
  DT=float(get_param(PFILE,'DT'))
  T=float(get_param(PFILE,'TEMPERATURE'))
  GG=float(get_param(PFILE,'GAMMA'))
  H=float(get_param(PFILE,'HEIGHT'))

  # Green-Kubo conversion factor
  fac=DT/T

  # theory prediction for xx and zz, Eq. 22, the spring constant is hard-coded (!)
  ctp = np.sqrt(10)
  th=[]
  th.append( 4.12*GG / (ctp**4*H**7) )
  th.append( ctp**-3 * (23.4/H**6 + 36.5*GG/(ctp*H**7)) )
  print(th)

  # read time series = 2 column file: FX, FZ
  fname=glob.glob(dir+'/timeseries.*') [0]
  data=np.loadtxt(fname).T

  # loop over XX, ZZ signal
  for i in range(2):
    # timeseries, subtract mean, compute correlation
    a=data[i]
    a-=np.mean(a)
    n=len(a)
    factor = np.arange(n,0,-1)[:n//2]
    cor = sp.correlate(a,a,mode='same',method='fft')[n//2:]/factor
    # running integral of correlation; multiplied by fac so that it is friction
    y = fac*np.cumsum(cor)
    # plot running integral, show theory prediction as horizontal line
    ax=fig.axes[2*fignum+i]
    if i==0: title = r'$\Gamma_{xx}$, '
    else: title = r'$\Gamma_{zz}$, '
    title += r'$h={}$, $\gamma={}$'.format(H,GG)
    ax.set_title(title)
    ax.set_xscale('log')
    ax.plot(y)
    ax.axhline(th[i],ls='--',c='r')

# loop over all directories, make the plots
for i,d in enumerate(dirs): analyze_datadir(d,i)

plt.tight_layout()

# plt.show()

# save plot
if getpass.getuser()=='rvink1':
  outputdir = os.path.expanduser('~') + '/www/'
  print('saving to',outputdir)
  plt.savefig(outputdir+'test.png',dpi=300)
  os.system('sh ' + outputdir + 'permissions.sh')
  exit()
