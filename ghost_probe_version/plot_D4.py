#!/usr/bin/env python3

'''
Use this tool to plot the D4 files.
'''

import sys,glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# set range of height values to use
hmin=2.5
hmax=9.5

# data directories to show
dirs=sys.argv[1:]

# total number of data sets
n=len(dirs)

# assign unique color to each data set
colors=cm.rainbow(np.linspace(0,1,n))

# make the plot

plt.xlabel(r'$h/\gamma$')
plt.ylabel(r'$\Gamma_{xx,zz} \times h^7 / \gamma$')

alldata=[]

for i in range(n):

  # read D4 file as dataframe; height value as index
  df = pd.read_csv(dirs[i]+'/D4',delim_whitespace=True,header=None,index_col=0)

  # ignore low and high height values
  df = df[(df.index>hmin)*(df.index<hmax)]

  # get the dpd gamma value
  g = np.asarray(df[1]) [0]

  # add the columns we want to plot to the dataframe

  # scaled height variable = height / gamma_dpd
  df['hs'] = df.index/g

  # scaled \Gamma_xx with error estimate
  df['xx'] = df[2]*df.index**7/g
  df['dxx'] = df[3]*df.index**7/g

  # scaled \Gamma_zz with error estimate
  df['zz'] = df[4]*df.index**7/g
  df['dzz'] = df[5]*df.index**7/g

  # keep all data for final fit
  alldata.append(df)

  # plot \Gamma_xx
  title = r'$\Gamma_{{xx}}$, $\gamma={}$'.format(g)
  plt.errorbar(df['hs'],df['xx'],yerr=df['dxx'],fmt='o',c=colors[i],label=title)

  # plot \Gamma_zz
  title = r'$\Gamma_{{zz}}$, $\gamma={}$'.format(g)
  plt.errorbar(df['hs'],df['zz'],yerr=df['dzz'],fmt='s',c=colors[i],label=title)

# do final fit
alldata=pd.concat(alldata)
x=alldata['hs']
y=alldata['zz']
a,b=np.polyfit(x,y,1)
title = r'$a={:.4f}$, $b={:.4f}$'.format(a,b)

x=np.linspace(np.min(x),np.max(x))
plt.plot(x,a*x+b,'--',c='k',label=title)

a=alldata['xx'].mean()
title = r'avg={:.4f}'.format(a)
plt.axhline(a,ls='--',c='g',label=title)

title = 'datasets:'
for d in dirs: title+=' '+d
plt.title(title)

plt.legend()

plt.show()
