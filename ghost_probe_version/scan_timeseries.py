#!/usr/bin/env python3

import sys,glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
from scipy import integrate

SHOWPLOT=False
if len(sys.argv[1:])==1: SHOWPLOT=True

def get_param(fname,str):
  chk = "#{}".format(str)
  fp=open(fname)
  for line in fp:
    field=line.strip().split('=')
    if field[0]==chk:
      fp.close()
      print('PARAMETER',str,field[1])
      return field[1]
  print('ERROR: file',fname,'does not contain variable',str)
  exit()

# get run parameters
DT=float(get_param('PARAM','DT'))
T=float(get_param('PARAM','TEMPERATURE'))
GG=get_param('PARAM','GAMMA')
H=get_param('PARAM','HEIGHT')
# this converts the correlation integral (simple sum in my case) to actual friction
fac=DT/T

def analyze(timeseries):
  a=timeseries.copy()
  a-=np.mean(a)
  n=len(a)
  factor = np.arange(n,0,-1)[:n//2]
  cor = sp.correlate(a,a,mode='same',method='fft')[n//2:]/factor
  i1 = np.cumsum(cor)*fac
  i2 = integrate.cumtrapz(cor,initial=0,axis=0)*fac
  min=int(100/DT)
  max=int(500/DT)
  best=np.mean(i2[min:max])
  if SHOWPLOT:
    plt.plot(i1)
    plt.plot(i2)
    plt.axvline(min,ls='--',c='r')
    plt.axvline(max,ls='--',c='r')
    x,y=method_running_mean(timeseries)
    plt.plot(x,y,'o',c='r')
    plt.xscale('log')
    plt.axhline(best)
    plt.axhline(np.mean(y),ls='--',c='g')
    plt.show()
  return best

def method_running_mean(timeseries):
  tmax = np.asarray(np.linspace(0.2,1.0,50)*len(timeseries),dtype=int)
  tmax = np.where(tmax%2==0,tmax,tmax-1)
  vals=[]
  for t in tmax:
    a=timeseries.copy()
    a=a[:t]
    a-=np.mean(a)
    n=len(a)
    factor = np.arange(n,0,-1)[:n//2]
    cor = sp.correlate(a,a,mode='same',method='fft')[n//2:]/factor
    ft = np.real(np.fft.fft(cor))
    vals.append(ft[1]*fac)
  return tmax/2,np.asarray(vals)

# read data
try:
  fname=glob.glob('timeseries.*') [0]
  data=np.loadtxt(fname).T
  print('file:',fname)
except:
  print('file not found / incompatible format')
  exit()

# debug
# for ts in data: analyze(ts)
# exit()

# prepare output line
line=[H,GG]

# analyze xx,zz time series
for ts in data:
  vals=[]
  # analyze full series
  vals.append(analyze(ts))
  # also analyze split series, to estimate the uncertainty
  for p in np.split(ts,5): vals.append(analyze(p))
  line.append(np.mean(vals))
  line.append(np.std(vals)/2)

# dump line
sys.stdout = open('gamma4','w')
for v in line: print(v,end=' ')
print()
