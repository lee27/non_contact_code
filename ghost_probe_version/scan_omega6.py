#!/usr/bin/env python3

import sys,os,getpass
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

'''
CORE TOOL: read in a cor.X / last.X file (output of MD simulation) and convert
this into an 'om' file, which is used by other tools.

In addition, create 2-panel plot of \Gamma_{xx,zz} vs. omega

EXAMPLES:

scan_omega6.py cor.0 
scan_omega6.py last.0 
scan_omega6.py cor.0 20
scan_omega6.py cor.0 2.0

The second argument (optional) sets the omega-range WMAX of the plot.

If WMAX is an integer, only the first WMAX __bins__ of the data are displayed, 
including errorbars. This is useful if you want to accurately determine \Gamma
in the limit of zero omega. NOTE: If plotting file last.X you get no error bars,
since this file refers to a single time series.

If WMAX is a float, omega is plotted up to the value specified, without error 
bars. This is useful if you want to see a wider omega range. A smoothing 
filter is applied.

default: WMAX=int(10)
'''

def get_param(fname,str):
  chk = "#{}".format(str)
  fp=open(fname)
  for line in fp:
    field=line.strip().split('=')
    if field[0]==chk:
      fp.close()
      print('PARAMETER',str,field[1])
      return field[1]
  print('ERROR: file',fname,'does not contain variable',str)
  exit()

# get run parameters
DT=float(get_param('PARAM','DT'))
T=float(get_param('PARAM','TEMPERATURE'))
TMAX=float(get_param('PARAM','TMAX'))
MIRU=int(get_param('PARAM','MIRU'))
GG=get_param('PARAM','GAMMA')
H=get_param('PARAM','HEIGHT')
fac=DT/T

# these should be the smallest and largest omega values that are sampled
tm=TMAX/2
wmin=2*np.pi/tm
wmax=np.pi/DT
print('sampling parameters:',tm,wmin,wmax)

try:
  # user specified omega range
  USERMAX=sys.argv[2]
  if '.' in USERMAX: USERMAX=float(USERMAX)
  else: USERMAX=int(USERMAX)
except:
  # default value
  USERMAX=int(10)

# read data
try:
  data=np.loadtxt(sys.argv[1],usecols=(1,2,4,5)).T
except:
  print('file not found / incompatible format')
  exit()

# how many timeseries were averaged over
# ns=np.loadtxt('correlation.0',max_rows=1)
# print('nsamples',ns)

X=[]

fig,_=plt.subplots(1,2,figsize=(12,3))

# loop over bulk / shear data; make plot for each
for i in range(2):

  # create informative plot label
  if MIRU==0: base = 'cyclic, '
  if MIRU==1: base = 'non-cyclic, '
  base += r'$\delta t={}$, $\gamma={}$, $h={}$, $t_M={}$'.format(DT,GG,H,TMAX)

  # convert raw data to average and std. dev.
  c1=data[2*i]
  c2=data[2*i+1]
  n=c1.size//2
  c1=c1[:n]
  c2=c2[:n]
  c2=np.sqrt(c2-c1**2)
  c1*=fac/2
  c2*=fac/2
  c1[0]*=2
  c2[0]*=2

  # make array of sampled frequencies, also store it
  dw=TMAX/2; dw=2*np.pi/dw; 
  freq=np.arange(c1.size)*dw
  if i==0: X.append(freq)

  # store gamma values
  X.append(c1)
  X.append(c2)

  ax=fig.axes[i]
  title = base 
  ax.set_title(title)
  ax.set_xlabel(r'$\omega$')
  if i==0: ax.set_ylabel(r'$\Gamma_{XX}$')
  if i==1: ax.set_ylabel(r'$\Gamma_{ZZ}$')
  ax.ticklabel_format(axis='y',style='sci',scilimits=(0,0))

  if isinstance(USERMAX,int): max=USERMAX
  else: max=np.argmax(freq>USERMAX)

  x=freq[1:max]
  y=c1[1:max]
  e=c2[1:max]
  best = np.mean(y)

  if isinstance(USERMAX,int): 
    ax.errorbar(x,y,yerr=e/2,fmt='o')
    ax.axhline(best,ls='--',c='orange',lw=2,zorder=10,label='{:.3E}'.format(best))
    ax.legend()
  else:
    smooth = savgol_filter(y,41,3)
    ax.plot(x,smooth) 

plt.tight_layout()

'''
create the 'om' file = 5 column file:
$1 = omega
$2 = \Gamma_xx
$3 = std. deviation in \Gamma_xx
$4 = \Gamma_zz
$5 = std. deviation in \Gamma_zz
'''

X=np.asarray(X).T
np.savetxt('om',X)

# save plot
if getpass.getuser()=='rvink1':
  outputdir = os.path.expanduser('~') + '/www/'
  print('saving to',outputdir)
  plt.savefig(outputdir+'omega.png',dpi=300)
  os.system('sh ' + outputdir + 'permissions.sh')
  exit()
