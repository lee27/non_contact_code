#!/usr/bin/env python3
from lammps import lammps
from mpi4py import MPI
import scipy.signal as sp
import numpy as np
import sys
from functools import partial
from multiprocessing import Pool
from datetime import datetime
from timeit import default_timer as timer
import pickle


comm = MPI.COMM_WORLD
rank=comm.Get_rank()
size=comm.Get_size()
argv = sys.argv
if len(argv) != 2:
    print("Syntax: base.py in.*")
    sys.exit()

infile = sys.argv[1]

start=timer()
if rank ==0:
    START = datetime.now()
    with open('JOBINFO','a') as f:
        dt_string = START.strftime('%d-%m-%Y %H:%M:%S')
        print(dt_string, 'START', file=f)

def ff_correlate(force,avgforce):
    m = force.shape[0]
    dforce   = force-avgforce
    factor   = np.arange(m,0,-1)[:m//2]
    ffcor    = sp.correlate(dforce,dforce,
                            mode='same',method='fft')[m//2:]/factor
    return ffcor


lmp = lammps(name='miru')
lmp.file(infile)
run1=int(lmp.extract_variable('equil'))
ani =int(lmp.extract_variable('animation'))
dt  =lmp.extract_global('dt')
dx    = -1e-1
RUNID = int(lmp.extract_variable('runid'))
lmp.command('compute PRES all pressure NULL bond')
lmp.command('thermo {}'.format(int(run1//10)))


lmp.command('dump PCA all custom 10 traj.{}.gz id type x y z'.format(RUNID))
P = []
V = []
lmp.command('run {}'.format(int(run1//10)))
lmp.command('write_data init.{}_0'.format(RUNID))
P.append(lmp.extract_compute('PRES',0,0))
vtemp = np.array(lmp.extract_box()[1])-np.array(lmp.extract_box()[0])
V.append(vtemp[0]*vtemp[1]*vtemp[2])

lmp.command('change_box all z delta 0 {}'.format(dx))
lmp.command('run {}'.format(int(run1)))
lmp.command('write_data init.{}_1'.format(RUNID))
P.append(lmp.extract_compute('PRES',0,0))
vtemp = np.array(lmp.extract_box()[1])-np.array(lmp.extract_box()[0])
V.append(vtemp[0]*vtemp[1]*vtemp[2])

lmp.command('change_box all z delta 0 {}'.format(dx))
lmp.command('run {}'.format(int(run1)))
lmp.command('write_data init.{}_2'.format(RUNID))
P.append(lmp.extract_compute('PRES',0,0))
vtemp = np.array(lmp.extract_box()[1])-np.array(lmp.extract_box()[0])
V.append(vtemp[0]*vtemp[1]*vtemp[2])

lmp.command('change_box all z delta 0 {}'.format(dx))
lmp.command('run {}'.format(int(run1)))
lmp.command('write_data init.{}_3'.format(RUNID))
P.append(lmp.extract_compute('PRES',0,0))
vtemp = np.array(lmp.extract_box()[1])-np.array(lmp.extract_box()[0])
V.append(vtemp[0]*vtemp[1]*vtemp[2])

lmp.command('change_box all z delta 0 {}'.format(dx))
lmp.command('run {}'.format(int(run1)))
lmp.command('write_data init.{}_4'.format(RUNID))
P.append(lmp.extract_compute('PRES',0,0))
vtemp = np.array(lmp.extract_box()[1])-np.array(lmp.extract_box()[0])
V.append(vtemp[0]*vtemp[1]*vtemp[2])

#P = np.array(P)
#V = np.array(V)

if rank == 0:
    print(P)
    #np.savetxt('pressure.data',P)
    #np.savetxt('volume.data',V)


MPI.Finalize()

END = datetime.now()
end = timer()
elaps = end - start
HOUR  = elaps//3600
MIN  = (elaps - (HOUR*3600))//60
SEC  = elaps - (HOUR*3600) - (MIN*60)
if rank ==0:
    with open('JOBINFO','a') as f:
        dt_string = END.strftime('%d-%m-%Y %H:%M:%S')
        print(dt_string, 'FINISH', 'WALLTIME:{0:.0f}h {1:.0f}m {2:.0f}s'.format(HOUR,MIN,SEC), file=f)
exit()
