*.in       : Sets up the system and defines parameters\
*.py       : Defines necessary observables and runs the simulation\
eval.py    : Reads parameters from a PARAM file and place into *.in file\
PARAM.base : Parameter list. The extension should be removed to be used by eval.py\
\
base       : Measuring the friction\
elastic    : Measuring the bulk elasiticity\
\
**LAMMPS version is (10 Feb 2021)**\
\
Below snippet is lines of commands to submit a job to the gwdg HPC cluster (sbatch).\
Be aware that the cpu architecture (here is cascadelake) must be identical to the one with which you compiled your LAMMPS.\
Please change the directory names and variables according to your system and useage.
```bash
#!/bin/bash
#SBATCH -p medium
#SBATCH -t 24:00:00
#SBATCH -n 20
#SBATCH -N 1
#SBATCH -C cascadelake

module load openmpi python fftw

export PATH=$HOME/non_contact_code:$HOME/bin:$HOME/.local/bin:/usr/local/bin:$HOME/MYPYTHON:$PATH
export PYTHONPATH=$HOME/.local/lib/python3.9/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$HOME/.local/lib64:$LD_LIBRARY_PATH

RUNID=$RANDOM
eval.py $HOME/non_contact_code/in.base $RUNID
mpiexec -n 20 base.py in.$RUNID
```
