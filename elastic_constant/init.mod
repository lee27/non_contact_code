# NOTE: This script can be modified for different atomic structures, 
# units, etc. See in.elastic for more info.

# VINK: we need bonds (!)
atom_style bond
bond_style dpd

# Define the finite deformation size. Try several values of this
# variable to verify that results do not depend on it.

#variable up equal 1.0e-6
variable up equal 1.0e-6
 
# Define the amount of random jiggle for atoms
# This prevents atoms from staying on saddle points
variable atomjiggle equal 1.0e-5

# Uncomment one of these blocks, depending on what units
# you are using in LAMMPS and for output

# metal units, elastic constants in eV/A^3
#units		metal
#variable cfac equal 6.2414e-7
#variable cunits string eV/A^3

# metal units, elastic constants in GPa
# units		metal
# variable cfac equal 1.0e-4
# variable cunits string GPa

# real units, elastic constants in GPa
#units		real
#variable cfac equal 1.01325e-4
#variable cunits string GPa

# VINK: use LJ units -> I HAVE NO IDEA OF THE CFAC CONVERSION, SO I PUT IT TO ONE (!!)
units lj
variable cfac equal 1.0
variable cunits string LJ-units

# Define minimization parameters
variable etol equal 0.0 
variable ftol equal 1.0e-10
variable maxiter equal 100
variable maxeval equal 1000
variable dmax equal 1.0e-2

# generate the box and atom positions using a diamond lattice
# VINK: changed to simple cubic lattice, unit lattice constant (!)

variable a equal 1.0
boundary	p p p
lattice         sc $a

# VINK: set system size here, make sure to allow for enough bonds (!)
region		box prism 0 10 0 10 0 10 0.0 0.0 0.0
create_box	1 box bond/types 2 extra/bond/per/atom 18
create_atoms	1 box

# Need to set mass to something, just to satisfy LAMMPS
# VINK: I put it to one (!)
mass 1 1.0

