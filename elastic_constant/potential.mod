# NOTE: This script can be modified for different pair styles 
# See in.elastic for more info.

# Choose potential
pair_style zero 4.0
pair_coeff * * 4.0

# VINK: create the bonds (!)

# VINK: we do T=0 calculation, so velocities should not be needed (!)
# comm_modify vel yes

# VINK: Let's be careful and not do this here (!)
# neigh_modify once yes

# MIRU bond stiffness (!)
variable STIFF1 equal 10

# @ MIRU: I noticed lammps by itself can also do direct evaluation of equations in script (no need to use @)
bond_coeff 1 1 $(v_STIFF1/2) 0 0 0.0 -1 666
bond_coeff 2 $(sqrt(2)) $(v_STIFF1/2) 0 0 0.0 -1 666

create_bonds many all all 1 0.99 1.01
create_bonds many all all 2 $(sqrt(2)-0.01) $(sqrt(2)+0.01)

# so you can look at it
write_data INIT.OVITO

# Setup neighbor style
neighbor 1.0 nsq
neigh_modify once no every 1 delay 0 check yes

# Setup minimization style
min_style	     cg
min_modify	     dmax ${dmax} line quadratic

# Setup output
thermo		5
thermo_style custom step temp pe press pxx pyy pzz pxy pxz pyz lx ly lz vol
thermo_modify norm no
